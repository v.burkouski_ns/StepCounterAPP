This is a test project for the step counting building on react-native [APP on Expo](https://expo.io/@bvs_nord/ns-step-tracker).



## Requirements

- [NodeJs](https://nodejs.org/en/).
- [Expo](https://expo.io/learn)
```
npm install exp --global
```
- [Yarn](https://yarnpkg.com/en/docs/install#mac-stable)

## Deployment
```
yarn
yarn start
```

