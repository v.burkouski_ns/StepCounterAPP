import React from 'react';

import {Scene, Router} from 'react-native-router-flux';

import * as Screens from './screens';
import {bootstrap} from './config/bootstrap';

import {AppLoading, Font} from 'expo';


bootstrap();




export default class App extends React.Component {
  state = {
    loaded: false
  };

  componentWillMount() {
    this._loadAssets();
  }

  _loadAssets = async() => {
    await Font.loadAsync({
      'fontawesome': require('./assets/fonts/fontawesome.ttf'),
      'icomoon': require('./assets/fonts/icomoon.ttf'),
      'Righteous-Regular': require('./assets/fonts/Righteous-Regular.ttf'),
      'Roboto-Bold': require('./assets/fonts/Roboto-Bold.ttf'),
      'Roboto-Medium': require('./assets/fonts/Roboto-Medium.ttf'),
      'Roboto-Regular': require('./assets/fonts/Roboto-Regular.ttf'),
      'Roboto-Light': require('./assets/fonts/Roboto-Light.ttf'),
    });
    this.setState({loaded: true});
  };

  render() {
    if (!this.state.loaded) {
      return <AppLoading />;
    }

    return <Router>
      <Scene key="root">
        <Scene key="splashScreen" component={Screens.SplashScreen}/>
        <Scene key="introduceScreen" component={Screens.WalkthroughScreen}/>
        <Scene key="stepTrackerScreen" component={Screens.StepTrackerScreen} title="Step Tracker"/>
      </Scene>
    </Router>
  }
}

Expo.registerRootComponent(App);
