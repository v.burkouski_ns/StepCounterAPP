import React from 'react';

import {Pedometer} from "expo";
import {ScrollView, View, AppState, AsyncStorage} from 'react-native';
import {RkStyleSheet, RkTheme, RkText} from 'react-native-ui-kitten';
import {FontAwesome} from '../../assets/icons';
import {KittenTheme} from '../../config/theme';
import {ProgressChart} from '../../components/';

export class StepTrackerScreen extends React.Component {

    constructor(props) {
        super(props);
        this.goalPaymentSteps = 1000;
        this.price = 0.01;
        this.trackerStartDate = new Date();
        this.state = {
            appState: AppState.currentState,
            isPedometerAvailable: "checking",
            pastStepCount: 0,
            dailyStepsCount: 0,
            paymentProgress: 0,
            dailyBalance: 0,
            totalBalance: 0,
            totalStepsCount: 0
        };
    }

    componentDidMount() {
         this._getTrackerStartDate().then((data) => {
            this.trackerStartDate = new Date(data);
            console.log(this.trackerStartDate);
            this._subscribe();
            this._getDailyProgress();
            this._getTotalProgress();
            AppState.addEventListener('change', this._handleAppStateChange);
        });

    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this._subscribe();
            this._getDailyProgress();
            this._getTotalProgress();
            console.log('App has come to the foreground!')
        }
        else {
            this._unsubscribe()
        }
        this.setState({appState: nextAppState});
    };

    _subscribe = () => {
        this._subscription = Pedometer.watchStepCount(result => {
            this._getDailyProgress();
            this._getTotalProgress();
        });


        Pedometer.isAvailableAsync().then(
            result => {
                this.setState({
                    isPedometerAvailable: String(result)
                });
            },
            error => {
                this.setState({
                    isPedometerAvailable: "Could not get isPedometerAvailable: " + error
                });
            }
        );

    };

    _unsubscribe = () => {
        this._subscription && this._subscription.remove();
        this._subscription = null;
    };

    _getDailyProgress() {
        let endDate = new Date();
        let startDate = new Date();
        startDate.setHours(0, 0, 0, 0);
        endDate.setHours(23, 59, 59, 999);
        if(startDate < this.trackerStartDate) {
            startDate = this.trackerStartDate
        }


        Pedometer.getStepCountAsync(startDate, endDate).then(
            result => {
                this.setState({dailyStepsCount: result.steps}, this._calculateDailylBalance);
            },
            error => {
                // TODO: handle error
            }
        );
    }

    _calculateDailylBalance() {
        let {dailyStepsCount} = this.state;
        let balance = (dailyStepsCount - dailyStepsCount % this.goalPaymentSteps) / this.goalPaymentSteps * this.price;
        this.setState({dailyBalance: balance})
    }


    _getTotalProgress() {

        let startDate = this.trackerStartDate;
        let endDate = new Date();
        Pedometer.getStepCountAsync(startDate, endDate).then(
            result => {
                this.setState({totalStepsCount: result.steps}, this._calculateTotalBalance);
            },
            error => {
                console.error(error)
            }
        );

    }

    _calculateTotalBalance() {
        let {totalStepsCount} = this.state;
        let balance = (totalStepsCount - totalStepsCount % this.goalPaymentSteps) / this.goalPaymentSteps * this.price;
        this.setState({totalBalance: balance})
    }


    _getNextPaymentProgress() {
        let {dailyStepsCount} = this.state;
        return dailyStepsCount % this.goalPaymentSteps
    }

    async _getTrackerStartDate() {
        try {
            let trackerStartDate = await AsyncStorage.getItem('trackerStartDate');
            return JSON.parse(trackerStartDate)
        }
        catch (error) {
            console.error(error)
        }
    }

    render() {
        let chartBlockStyles = [styles.chartBlock, {backgroundColor: RkTheme.current.colors.control.background}];
        return (
            <ScrollView style={styles.screen}>
                <View style={styles.statItems}>
                    <View style={[styles.statItemContainer, {backgroundColor: RkTheme.current.colors.dashboard.stars}]}>
                        <View>
                            <RkText rkType='header6' style={styles.statItemValue}>{this.state.totalStepsCount}</RkText>
                            <RkText rkType='secondary7' style={styles.statItemName}>Steps</RkText>
                        </View>
                        <RkText rkType='awesome hero' style={styles.statItemIcon}>{FontAwesome['transport']}</RkText>
                    </View>
                    <View style={[styles.statItemContainer, {backgroundColor: RkTheme.current.colors.dashboard.tweets}]}>
                        <View>
                            <RkText rkType='header6' style={styles.statItemValue}>{this.state.totalBalance}</RkText>
                            <RkText rkType='secondary7' style={styles.statItemName}>Total balance</RkText>
                        </View>
                        <RkText rkType='awesome hero' style={styles.statItemIcon}>{FontAwesome['money']}</RkText>
                    </View>
                </View>
                <View style={chartBlockStyles}>
                    <ProgressChart title={'Progress until the next payment'} goalValue={this.goalPaymentSteps}
                                   currentValue={this._getNextPaymentProgress()}
                                   chartTitle={'Today you have reached:'}
                                   totalValue={'Steps: ' + this.state.dailyStepsCount}
                                   bonusValue={'balance: +' + this.state.dailyBalance + '$'}/>
                </View>

            </ScrollView>
        );
    }
}

let styles = RkStyleSheet.create(theme => ({
    container: {
        backgroundColor: KittenTheme.colors.screen.base,
        justifyContent: 'space-between'
    },
    screen: {
        marginVertical: 15,
        backgroundColor: theme.colors.screen.scroll,
        paddingHorizontal: 15,
    },
    chartBlock: {
        padding: 15,
        marginBottom: 15,
        justifyContent: 'center'
    },
    statItems: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 15,
    },
    statItemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 3,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    statItemIcon: {
        alignSelf: 'center',
        marginLeft: 10,
        color: 'white',
    },
    statItemValue: {
        color: 'white',
    },
    statItemName: {
        color: 'white',
    },

}));