import React from 'react';
import { View, AsyncStorage } from 'react-native';
import {Actions} from 'react-native-router-flux'
import {RkStyleSheet} from 'react-native-ui-kitten';
import {GradientButton} from '../../components/';
import {Walkthrough} from '../../components/walkthrough';
import {Walkthrough1} from './walkthrough1';
import {Walkthrough2} from './walkthrough2';
import {PaginationIndicator} from '../../components';


export class WalkthroughScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {index: 0};
  }

  _changeIndex(index) {
    this.setState({index})
  }

  async _setTrackerStatus() {
    try {
        return await AsyncStorage.setItem('trackerStarted', JSON.stringify(true));
    } catch (error) {
        console.error('AsyncStorage#setItem error: ' + error.message);
    }

  }

  async _setStartTrackingDate() {
    try {
        return await AsyncStorage.setItem('trackerStartDate', JSON.stringify(new Date()));
    } catch (error) {
        console.error('AsyncStorage#setItem error: ' + error.message);
    }
  }

  render() {
    return (
      <View style={styles.screen}>
        <Walkthrough onChanged={(index) => this._changeIndex(index)}>
          <Walkthrough1/>
          <Walkthrough2/>
        </Walkthrough>
        <PaginationIndicator length={2} current={this.state.index}/>
        <GradientButton
          rkType='large'
          style={styles.button}
          text="Start Tracking"
          onPress={() => {
            this._setTrackerStatus();
            this._setStartTrackingDate();
            Actions.stepTrackerScreen()
          }}/>
      </View>
    )
  }
}

let styles = RkStyleSheet.create(theme => ({
  screen: {
    backgroundColor: theme.colors.screen.base,
    paddingVertical: 28,
    alignItems: 'center',
    flex: 1,
  },
  button: {
    marginTop: 25,
    marginHorizontal: 16,
  }
}));