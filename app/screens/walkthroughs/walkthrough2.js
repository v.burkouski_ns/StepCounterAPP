import React from 'react';
import { StyleSheet, Image, View, Dimensions } from 'react-native';
import { RkText, RkStyleSheet, RkTheme } from 'react-native-ui-kitten';
import SvgUri from 'react-native-svg-uri';
import {scaleVertical} from '../../utils/scale';

let logo = require('../../assets/images/nord-soft-logo.svg');

export class Walkthrough2 extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let width = Dimensions.get('window').width;
    let image = <SvgUri width={width-40} height={scaleVertical(300)} source={logo} />

    return (
      <View style={styles.screen}>
        {image}
        <RkText rkType='header2' style={styles.text}>Make activity and earn money</RkText>
      </View>
    )
  }
}

let styles = RkStyleSheet.create(theme => ({
  screen: {
    backgroundColor: theme.colors.screen.base,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  text: {
    textAlign: 'center',
    marginTop: 20,
    marginHorizontal: 30
  }
}));