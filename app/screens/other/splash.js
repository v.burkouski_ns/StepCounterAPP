import React from 'react';
import {StyleSheet, View, Dimensions, StatusBar, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux'
import {RkText, RkTheme} from 'react-native-ui-kitten'
import SvgUri from 'react-native-svg-uri';

import {ProgressBar} from '../../components';
import {KittenTheme} from '../../config/theme';

import {scale, scaleVertical} from '../../utils/scale';

let timeFrame = 500;
let logo = require('../../assets/images/nord-soft-logo.svg');

export class SplashScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            progress: 0
        }
    }

    componentDidMount() {
        StatusBar.setHidden(false, 'none');
        RkTheme.setTheme(KittenTheme);

        this.timer = setInterval(() => {
            if (this.state.progress === 1) {
                clearInterval(this.timer);
                setTimeout(() => {
                    AsyncStorage.getItem('trackerStarted').then((data) => {
                        if (data) {
                            Actions.stepTrackerScreen()
                        }
                        else {
                            Actions.introduceScreen()
                        }
                    });
                    //trackerStarted ? Actions.stepTrackerScreen() : Actions.introduceScreen();
                }, timeFrame);
            } else {
                let random = Math.random() * 0.5;
                let progress = this.state.progress + random;
                if (progress > 1) {
                    progress = 1;
                }
                this.setState({progress});
            }
        }, timeFrame)

    }

    render() {
        let width = Dimensions.get('window').width;
        return (

            <View style={styles.screen}>
                <View style={styles.container}>

                    <SvgUri
                        width={width - 40}
                        height={scaleVertical(430)}
                        source={logo}
                    />
                    <View style={styles.text}>
                        <RkText rkType='logo' style={styles.appName}>Simple Step Tracker</RkText>
                    </View>
                </View>
                <ProgressBar
                    color={RkTheme.current.colors.accent}
                    style={styles.progress}
                    progress={this.state.progress} width={scale(320)}/>
            </View>

        )
    }
}

let styles = StyleSheet.create({
    screen: {
        backgroundColor: KittenTheme.colors.screen.base,
        paddingVertical: 28,
        alignItems: 'center',
        flex: 1,
    },
    container: {
        alignItems: 'center',
    },
    image: {
        resizeMode: 'cover',
        height: scaleVertical(430),
    },
    text: {
        marginBottom: 20,
        alignItems: 'center'
    },
    hero: {
        marginBottom: 20,
        fontSize: 36,
    },
    appName: {
        fontSize: 38,
    },
    progress: {
        alignSelf: 'center',
        marginBottom: 35,
        backgroundColor: '#e5e5e5'
    }
});