import React from 'react';
import { View, Dimensions } from 'react-native';
import { RkComponent, RkTheme, RkText } from 'react-native-ui-kitten';
import {LineChart, YAxis, Grid} from 'react-native-svg-charts'


export class AreaChart extends RkComponent {

    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        const data = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80]

        const contentInset = {top: 20, bottom: 20}

        return (
            <View>
                <RkText rkType='header4'>{this.props.title}</RkText>
                <View style={{height: 200, flexDirection: 'row'}}>
                    <YAxis
                        data={data}
                        contentInset={contentInset}
                        svg={{
                            fill: 'grey',
                            fontSize: 10,
                        }}
                        numberOfTicks={10}
                        formatLabel={value => `${value}ºC`}
                    />
                    <LineChart
                        style={{flex: 1, marginLeft: 16}}
                        data={data}
                        svg={{stroke: 'rgb(134, 65, 244)'}}
                        contentInset={contentInset}
                    >
                        <Grid/>
                    </LineChart>
                </View>
            </View>
        )
    }
}
